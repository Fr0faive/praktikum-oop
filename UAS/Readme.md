# NO. 1

## Usecase User:
    1. Login dan mengakses digital banking web service perusahaan.
    2. Melihat saldo rekening perusahaan dan informasi transaksi terkini.
    3. Melakukan transfer antar rekening perusahaan.
    4. Mengelola tagihan dan pembayaran perusahaan.
    5. Melihat laporan keuangan perusahaan.
    6. Mengatur notifikasi dan pemberitahuan terkait transaksi.

## Usecase Manajemen Perusahaan:
    1. Mengakses dashboard manajemen perusahaan melalui digital banking web service.
    2. Melihat dan mengelola saldo rekening perusahaan secara keseluruhan.
    3. Melakukan analisis keuangan perusahaan berdasarkan laporan dan data yang disediakan.
    4. Mengelola arus kas perusahaan.
    5. Mengatur dan melacak anggaran perusahaan.
    6. Mengelola dan memantau transaksi yang dilakukan oleh karyawan.
    7. Mengakses fitur penggajian dan manajemen keuangan karyawan.
    8. Mengelola persetujuan dan otorisasi transaksi yang dilakukan oleh karyawan.
    9. Melakukan pengelolaan risiko keuangan perusahaan.

## Usecase Direksi Perusahaan:
    1. Mengakses dashboard eksekutif melalui digital banking web service.
    2. Melihat ringkasan keuangan perusahaan secara keseluruhan.
    3. Memonitor kinerja dan pertumbuhan perusahaan.
    4. Melihat laporan keuangan dan analisis mendalam.
    5. Mengakses informasi strategis terkait keputusan bisnis.
    6. Mengelola proyeksi keuangan dan perencanaan jangka panjang perusahaan.
    7. Mengakses informasi pasar dan tren terkini.
    8. Mengatur dan memantau tingkat kepatuhan perusahaan terhadap peraturan dan kebijakan keuangan.
    9. Melakukan evaluasi risiko dan pengambilan keputusan berdasarkan analisis data.
