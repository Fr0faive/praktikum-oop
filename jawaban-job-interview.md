# No. 1
> Pendekatan matematika

![Pendekatan_matematika](https://gitlab.com/Fr0faive/praktikum-oop/-/raw/main/GIF/Pendekatan%20Matematika.gif)

# No. 2
> Demo untuk nomer 2

![No2](https://gitlab.com/Fr0faive/praktikum-oop/-/raw/main/GIF/No2.gif)

# No. 3
> Demo untuk nomer 3

![No3](https://gitlab.com/Fr0faive/praktikum-oop/-/raw/main/GIF/No3.gif)

# No. 4
> Demo untuk nomer 4

![No4](https://gitlab.com/Fr0faive/praktikum-oop/-/raw/main/GIF/no4.gif)

# No. 5
> Demo untuk nomer 5

![No5](https://gitlab.com/Fr0faive/praktikum-oop/-/raw/main/GIF/no5.gif)

# No. 6
> Demo untuk nomer 6

![No6](https://gitlab.com/Fr0faive/praktikum-oop/-/raw/main/GIF/no6.gif)

# No. 7
- Class User
> UseCase: Register
> 1. User memilih menu register
> 2. User memasukkan username
> 3. User memasukkan password
> 4. User memasukkan nominal tabungannya
> 5. User berhasil mendaftar
> 6. Sistem memunculkan informasi berhasil register

> UseCase: Login
> 1. User memilih menu login
> 2. User memasukkan username
> 3. User memasukkan password
> 4. User mengkonfirmasi login atau keluar
> 5. User berhasil login
> 6. Sistem menampilkan dashbord menu

> UseCase: Transfer
> 1. User memilih menu transfer
> 2. User memasukan tujuan transfer (user)
> 3. User memasukan jumlah transfer
> 4. User berhasil mentransfer

> UseCase: Bayar Tagihan Listrik
> 1. User memilih menu bayar tagihan listrik
> 2. Sistem menampilkan jumlah tagihan yang harus dibayar
> 3. User mengkonfirmasi untuk membayar atau tidak
> 4. User mengisi password untuk autentikasi
> 5. Sistem menampilkan informasi bahwa tagihan berhasil dibayar

> UseCase: Logout
> 1. User memilih menu Logout
> 2. Sistem menampilkan halaman login

# No. 8
> Class Diagram

```mermaid
---
title: BNIAppMobile
---
classDiagram
    Bank <|-- BniBank
    Bank "1" -- "1" Bills
    Bank "1" -- "1" User
    Bank "1" -- "0..n" UserList
    User "1" -- "1" ElectricityBill
    App "1" -- "1" Menu
    Menu "1" -- "0..n" UserList
    <<abstract>> Bank
    Bank : -String name
    Bank : -String balance
    Bank: +getName() String
    Bank: +getBalance() int
    Bank: +transfer()* abstract void
    class BniBank{
        +BniBank(int balance)
        +transfer(int amount) void
    }

    Bills <|-- ElectricityBill
    <<abstract>> Bills
    Bills : -String nama
    Bills : -int amount
    Bills : -bool isPaid
    Bills : +getNama() String
    Bills : +getAmount() int
    Bills : +isPaid() bool
    Bills : +paidSign() void
    class ElectricityBill {
        +ElectricityBill(int balance)
    }

    User "1" -- "1" UserList

    User : -String username
    User : -String password
    User : -Bank bank
    User : +User(username, password, bank)
    User : +User(username, password, bank, electricityBill)
    User : +getTotalBill() int
    User : +getIsPaid() bool
    User : +payElectricityBill() void
    User : +authenticate(password) void
    User : +transfer(User userLain, int amount) void
    User : +getNameBank() String
    User : +getBalance() int
    User : +getUsername() String
    class UserList{
        -List~User~ users
        +UserList()
        +addUser() void
        +authenticate() bool
        +getUser() User
        +transfer() void
        +billElectric() void
        +register() void
    }
    

    class Menu {
        +menuLogin() void
        +mainMenu() void
    }

    class App {
        +main()$ void
    }

```
> UseCase Table

| key | Usecase | Priority | Status |
| --- | ------- | -------- | ------ |
| 1 | Sebagai user, saya ingin mendaftar akun baru | Tinggi | Completed |
| 2 | Sebagai user, saya ingin bisa login menggunakan akun pribadi | Tinggi | Completed |
| 3 | Sebagai user, saya ingin bisa transfer untuk akun lain sesama bank | Tinggi | Completed |
| 4 | Sebagai user, saya ingin bisa transfer untuk akun lain di luar bank | Rendah | Planned |
| 5 | Sebagai user, saya ingin bisa membayar tagihan listrik | Sedang | Completed |
| 6 | Sebagai user, saya ingin bisa membayar tagihan internet | Sedang | Planned |
| 7 | Sebagai user, saya ingin bisa membayar tagihan kartu pascabayar | rendah | Unplanned |
| 8 | Sebagai user, saya ingin bisa top-up e-wallet | rendah | Unplanned |
| 9 | Sebagai user, saya ingin bisa memutasi rekening | rendah | Unplanned |

# No. 9
Youtube : 
[Link Youtube](https://youtu.be/oh5gbJHpV5s)

# No. 10
- Menu awal

![MenuAwal](https://gitlab.com/Fr0faive/praktikum-oop/-/raw/main/GIF/Screenshot%202023-05-12%20182843.png)

- Daftar

![Daftar](https://gitlab.com/Fr0faive/praktikum-oop/-/raw/main/GIF/Daftar.png)

- Login

![Login](https://gitlab.com/Fr0faive/praktikum-oop/-/raw/main/GIF/Login.png)

- Dashboard

![Dashboard](https://gitlab.com/Fr0faive/praktikum-oop/-/raw/main/GIF/Dashboard.png)

- Transfer

![Transfer](https://gitlab.com/Fr0faive/praktikum-oop/-/raw/main/GIF/Transfer.png)

- Bayar Tagihan

![Bayar Tagihan](https://gitlab.com/Fr0faive/praktikum-oop/-/raw/main/GIF/BayarTagihan.png)

