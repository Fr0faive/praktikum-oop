package Bills;
// Class tagihan digunakan untuk parent segala macam tagihan
public abstract class Bills {
    private String nama;
    private int amount;
    private boolean isPaid;

    public Bills(String nama, int amount) {
        this.nama = nama;
        this.amount = amount;
        this.isPaid = false;
    }

    public String getName() {
        return nama;
    }

    public int getAmount() {
        return amount;
    }

    public boolean isPaid() {
        return isPaid;
    }

    // memberi tanda status tagihan
    public void paidSign() {
        isPaid = true;
        System.out.println("Pembayaran tagihan " + nama + " sebesar Rp" + amount + " berhasil dilakukan.");
    }
}