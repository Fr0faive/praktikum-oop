import Menu.Menu;

// Class utama menjalankan program
// === Informasi ===
// Daftar terlebih dahulu jika tidak mempunyai akun

public class App{
    public static void main(String[] args) {
        Menu menu = new Menu();
        menu.mainMenu();
    }
}
