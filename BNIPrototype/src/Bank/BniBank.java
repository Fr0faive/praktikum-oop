package Bank;
public class BniBank extends Bank {        // inheritance
    public BniBank(int balance) {
        super("BNI", balance);
    }
    // polymorphism override
    // Mengoverride transfer
    @Override
    public void transfer(int amount) {
        if (balance >= amount) {
            balance -= amount;
        } else {
            System.out.println("Transfer gagal. Saldo Kurang.");
        }
    }
}