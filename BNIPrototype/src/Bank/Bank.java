package Bank;
public abstract class Bank {           //abstract
    protected String name;
    protected int balance;

    public Bank(String name, int balance) {
        this.name = name;
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public int getBalance() {
        return balance;
    }

    // Menambahkan Saldo
    public void addBalance(int amount) {
        balance += amount;
    }

    // method abstract transfer
    public abstract void transfer(int amount);
}