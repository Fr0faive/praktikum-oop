package Menu;
import java.util.Scanner;

import User.UserList;

// Class Dashboard
public class Menu {
    protected UserList userList = new UserList();
    Scanner scanner = new Scanner(System.in);
    Scanner scannerInt = new Scanner(System.in);

    // Menu Dashboard
    public void mainLogin() {
        boolean putar = true;
        System.out.println("Selamat datang di BNIApp!");
        System.out.print("Masukkan Username: ");
        String username = scanner.nextLine();
        System.out.print("Masukkan password: ");
        String password = scanner.nextLine();
        System.out.print("Login? (y = login, x = keluar)");
        String choice = scanner.nextLine();
        if (choice.equalsIgnoreCase("y")) {
            if (userList.authenticate(username, password)) {
                do {
                    System.out.println("=== Data Diri ===");
                    System.out.println("Nama: " + userList.getUser(username).getUsername());
                    System.out.println("Bank: " + userList.getUser(username).getBankName());
                    System.out.println("Saldo: " + userList.getUser(username).getBalance());
                    System.out.println(
                            "==== MENU ====\n1. Transfer\n2. Bayar Tagihan Listrik\n3. Logout\nPilih menu: ");
                    int menu = scannerInt.nextInt();
                    switch (menu) {
                        case 1:
                            System.out.print("Masukkan nama penerima: ");
                            String recipient = scanner.nextLine();
                            System.out.print("Jumlah yang ingin dikirim: ");
                            int amount = scannerInt.nextInt();
                            userList.transfer(username, recipient, amount);
                            break;
                        case 2:
                            if (userList.getUser(username).getIsPaid()) {
                                System.out.println("Tagihan sudah dibayar!");
                            } else {
                                userList.billElectric(userList.getUser(username));
                            }
                            break;
                        case 3:
                            putar = false;
                            break;
                        default:
                            System.out.println("Pilihan menu salah!");
                            break;
                    }
                } while (putar);
            } else {
                System.out.println("Username atau password salah!");
            }
        }
    }

    // Main Menu
    public void mainMenu() {
        boolean condition = false;

        while (true) {
            System.out.println("=== WELCOME ===");
            System.out.println("1. Login");
            System.out.println("2. Daftar");
            System.out.println("3. Keluar");
            System.out.println("Tidak punya akun? Daftar dulu");
            System.out.print("Pilih menu: ");
            int choice = scannerInt.nextInt();
            switch (choice) {
                case 1:
                    mainLogin();
                    break;
                case 2:
                    userList.register();
                    break;
                case 3:
                    condition = true;
                    break;
                default:
                    break;
            }

            if (condition){
                break;
            }
        }
    }
}
