package User;
import Bank.Bank;
import Bills.ElectricityBill;
import Bills.InternetBill;

// Class ini berisi atribut dan kebutuhan lain tentang user
public class User {
    private String username;
    private String password;
    private Bank bank;
    private ElectricityBill electricityBill;
    private InternetBill internetBill;

    public User(String username, String password, Bank bank) {
        this.username = username;
        this.password = password;
        this.bank = bank;
        this.electricityBill = new ElectricityBill(368000);
        this.internetBill = new InternetBill(256000);
    }

    // Overload
    public User(String username, String password, Bank bank, int electricityBillAmount) {
        this.username = username;
        this.password = password;
        this.bank = bank;
        this.electricityBill = new ElectricityBill(electricityBillAmount);
    }

    public void setElectricityBill(int amount) {
        electricityBill = new ElectricityBill(amount);
    }



    public int getTotalBill() {
        return electricityBill.getAmount();
    }

    public boolean getIsPaid() {
        return electricityBill.isPaid();
    }

    // Methode membayar tagihan listrik
    public void payElectricityBill() {
        if (electricityBill.isPaid()) {
            System.out.println("Tagihan listrik sudah dibayar sebelumnya.");
        } else {
            int amount = electricityBill.getAmount();
            if (bank.getBalance() >= amount) {
                bank.transfer(amount);
                electricityBill.paidSign();
            } else {
                System.out.println("Pembayaran tagihan listrik gagal. Saldo Anda tidak mencukupi.");
            }
        }
    }

    public void payInternetBill() {
        if(internetBill.isPaid()) {
            System.out.println("Tagihan internet sudah dibayar sebelumnya");
        } else {
            int amount = internetBill.getAmount();
            if(bank.getBalance()>=amount) {
                bank.transfer(amount);
                internetBill.paidSign();
            } else {
                System.out.println("Pembayaran tagihan listrik gagal. Saldo anda tidak mencukupi");
            }
        }
    }

    // Autentikasi Password User
    public boolean authenticate(String password) {
        return this.password.equals(password);
    }


    // Methode Transfer
    public void transfer(User userLain, int amount) {
        if (bank.getBalance() >= amount) {
            bank.transfer(amount);
            userLain.bank.addBalance(amount);;
            System.out.println("Transfer sukses.");
        } else {
            System.out.println("Transfer gagal. Saldo kurang");
        }
    }

    public String getBankName() {
        return bank.getName();
    }

    public int getBalance() {
        return bank.getBalance();
    }

    public String getUsername() {
        return username;
    }
}