package User;

import java.util.Scanner;
import java.util.LinkedList;
import Bank.BniBank;

// Class UserList berguna untuk menampung class User ke dalam arraylist
public class UserList {
    private LinkedList<User> users;
    Scanner scanner = new Scanner(System.in);
    Scanner scannerInt = new Scanner(System.in);

    public UserList() {
        this.users = new LinkedList<>();
    }

    public void addUser(User user) {
        users.add(user);
    }

    // autentikasi username & password
    public boolean authenticate(String username, String password) {
        for (User user : users) {
            if (user.getUsername().equals(username) && user.authenticate(password)) {
                return true;
            }
        }
        return false;
    }

    public User getUser(String username) {
        for (User user : users) {
            if (user.getUsername().equalsIgnoreCase(username)) {
                return user;
            }
        }
        return null;
    }

    // Metod mengirim uang
    public void transfer(String senderUsername, String recipientUsername, int amount) {
        User sender = getUser(senderUsername);
        User recipient = getUser(recipientUsername);

        System.out.print("Masukkan password: ");
            String password = scanner.nextLine();
            if(sender.authenticate(password)) {
                if (recipient == null) {
                    System.out.println("Penerima tidak ditemukan.");
                    return;
                } else {
                    sender.transfer(recipient, amount);
                }
            } else {
                System.out.println("Password salah!");
            }
    }


    // Method membayar tagihan listrik
    public void billElectric(User user) {
        System.out.println("Total tagihan anda: " + user.getTotalBill());
        System.out.println("Ingin membayarnya? (y/n)");
        String confirm = scanner.nextLine();
        if (confirm.equalsIgnoreCase("y")) {
            System.out.print("Masukkan password: ");
            String password = scanner.nextLine();
            if(user.authenticate(password)) {
                user.payElectricityBill();
            } else {
                System.out.println("Password salah!");
            }
        }
    }

    // Method Mendaftar
    public void register() {
        System.out.print("Masukkan username: ");
        String username = scanner.nextLine();

        // Memeriksa apakah username sudah digunakan
        for (User user : users) {
            if (user.getUsername().equals(username)) {
                System.out.println("Username sudah digunakan.");
                return;
            }
        }

        System.out.print("Masukkan password: ");
        String password = scanner.nextLine();

        System.out.print("Masukkan Saldo Awal: ");
        int balance = scannerInt.nextInt();

        // Membuat objek User baru dan menambahkannya ke dalam UserList
        BniBank bank = new BniBank(balance);
        User user = new User(username, password, bank);
        addUser(user);
        System.out.println("Pendaftaran berhasil.");
    }
}
